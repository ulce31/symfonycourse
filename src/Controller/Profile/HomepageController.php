<?php

namespace App\Controller\Profile;

use App\Entity\Gif;
use App\Form\GifType;
use App\Repository\GifRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\ByteString;


class HomepageController extends AbstractController
{

    private RequestStack $requestStack;
    private GifRepository $gifRepository;
    private EntityManagerInterface $entityManager;


    public function __construct(RequestStack $requestStack, GifRepository $gifRepository, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->gifRepository = $gifRepository;
        $this->entityManager = $entityManager;
    }

    /**
     *  @Route ("/profile", name="profile.homepage.index")
     */
    public function index()
    {
        $user = $this->getUser();
        $gifs = $this->gifRepository->getByUserId($user->getId())->getResult();

        return $this->render("profile/homepage/index.html.twig", ["gifs" => $gifs]);
    }

    /**
     * @Route("/profile/form", name="profile.homepage.form")
     */
    public function form(): Response
    {
        $model = new Gif();
        $type = GifType::class;
        $form = $this->createForm($type, $model);
        $form->handleRequest($this->request);
        if ($form->isSubmitted() &&  $form->isValid()) {
            $model->setUser($this->getUser());
            $model->setAuthor('');
            $imageName = ByteString::fromRandom(32)->lower();
            $imageExtension = $model->getName()->guessExtension();
            $model->getName();

            $model->getName()->move('img', "$imageName.$imageExtension");
            $model->setSlug("$imageName.$imageExtension");
            $model->setName("$imageName.$imageExtension");

            $this->entityManager->persist($model);
            $this->entityManager->flush();


            dd($model);
        }
        return $this->render('profile/homepage/form.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /** 
     * @Route ("/admin", name="homepage.admin.index" )
     */
    public function adminIndex(): Response
    {
        return $this->render("security/login.html.twig");
    }
}
