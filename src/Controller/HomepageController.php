<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends AbstractController
{

    private RequestStack $requestStack;


    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     *  @Route ("/", name="homepage.index")
     */
    public function index()
    {

        return $this->render("homepage/index.html.twig", ["message" => "coucou"]);
    }

    /**
     * @Route ("/admin", name="homepage.admin.index" )
     */
    public function adminIndex(): Response
    {
        return $this->render("security/login.html.twig");
    }
}
