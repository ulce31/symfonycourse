<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use function PHPSTORM_META\type;

class SearchController extends AbstractController
{
    private RequestStack $requestStack;
    private CategoryRepository $categoryRepository;

    public function __construct(RequestStack $requestStack, CategoryRepository $categoryRepository)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/search", name="search")
     */
    public function index()
    {
        $parents = [];
        $parentsWithChildrens = [];
        $results = $this->categoryRepository->findByName($this->request->request->get('category'))->getResult();
        foreach ($results as $result) {
            if ($result->getParent() == null) {
                array_push($parents, $result->getSlug());
            }
        }

        // Traitement du cas où il y a un parent 
        if ($parents != []) {
            foreach ($parents as $value) {
                array_push($parentsWithChildrens[$value], $this->categoryRepository->findChildrens($value));
            }
            $results->parents = $parentsWithChildrens;
        }


        return $this->render('search/index.html.twig', [
            'controller_name' => 'SearchController',
            'results' => $results
        ]);
    }
}
