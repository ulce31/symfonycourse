<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class CategoryController extends AbstractController
{
    private RequestStack $requestStack;
    private CategoryRepository $categoryRepository;

    public function __construct(RequestStack $requestStack, CategoryRepository $categoryRepository)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * @Route("/category/{slug}", name="category")
     */
    public function index($slug)
    {
        $catChildren = $this->categoryRepository->findChildrens($slug);
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            "parent_slug" => $slug,
            'cat_children' => $catChildren
        ]);
    }



    /**
     * @Route("/category/{categorySlug}/{subcategorySlug}", name="category.subcategory")
     */
    public function subCategory(string $categorySlug, string $subcategorySlug): Response
    {
        $subcategory = $this->categoryRepository->findOneBy(["slug" => $subcategorySlug]);
        $gifs = $subcategory->getGifs();
        return $this->render('category/subcategory.html.twig', [
            'subcategory' => $subcategory,
            'gifs' => $gifs
        ]);
    }
}
