<?php

namespace App\Controller;

use App\Repository\GifRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GifController extends AbstractController
{
    private GifRepository $gifRepository;

    public function __construct(GifRepository $gifRepository)
    {
        $this->gifRepository = $gifRepository;
    }

    /**
     * @Route("/gif/{slug}", name="gif")
     */
    public function index($slug)
    {
        $gif = $this->gifRepository->findOneBy(['slug' => $slug]);

        return $this->render('gif/index.html.twig', [
            'controller_name' => 'GifController',
            'gif' => $gif
        ]);
    }
}
