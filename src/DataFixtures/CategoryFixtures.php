<?php

namespace App\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\AbstractDataFixtures;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $slugger  = new AsciiSlugger();
        foreach (AbstractDataFixtures::CATEGORIES as $main => $sub) {
            $mainCategory = new Category();
            $mainCategory
                ->setName($main)
                ->setSlug($slugger->slug($main));
            $manager->persist($mainCategory);

            foreach ($sub as $subcategory) {
                $subcat = new Category();
                $subcat
                    ->setName($subcategory)
                    ->setSlug($slugger->slug($subcategory))
                    ->setParent($mainCategory);
                $manager->persist($subcat);
                $this->addReference("subcategory$subcategory", $subcat);
            }
        }


        $manager->flush();
    }
}
