<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findWithoutParent()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.parent IS NULL')
            ->getQuery()
            ->getResult();
    }

    public function findChildrens(string $slugCategory)
    {
        return $this->createQueryBuilder('category')
            ->join('category.parent', 'parent')
            ->where('parent.slug = :slug')
            ->setParameters([
                'slug' => $slugCategory
            ])
            ->getQuery()
            ->getResult();
    }

    public function findSubcategories(): QueryBuilder
    {
        return $this->createQueryBuilder('category')
            ->where('category.parent IS NOT NULL');
    }

    public function findGifsBySubcat($subcategorySlug)
    {
        return $this->createQueryBuilder('category')
            ->join('gif.category_id', 'parent')
            ->where('parent.slug = :slug')
            ->setParameters([
                'slug' => $subcategorySlug
            ])
            ->getQuery()
            ->getResult();
    }

    public function findByName($name): Query
    {
        return $this->createQueryBuilder('category')
            ->where('category.name LIKE :name')
            ->setParameters([
                'name' => "$name%"
            ])
            ->getQuery();
    }



    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
