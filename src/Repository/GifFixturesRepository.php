<?php

namespace App\Repository;

use App\Entity\GifFixtures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GifFixtures|null find($id, $lockMode = null, $lockVersion = null)
 * @method GifFixtures|null findOneBy(array $criteria, array $orderBy = null)
 * @method GifFixtures[]    findAll()
 * @method GifFixtures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GifFixturesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GifFixtures::class);
    }

    // /**
    //  * @return GifFixtures[] Returns an array of GifFixtures objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GifFixtures
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
